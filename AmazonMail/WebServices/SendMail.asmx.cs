﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;

using AmazonMail.Modules;

namespace AmazonMail.WebServices
{
    /// <summary>
    /// Summary description for SendMail
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SendMail : System.Web.Services.WebService
    {

        [WebMethod]
        public string Send(string strSecureCode, string sender, string receiver, string subject, string content, string appId)
        {
            string ip = HttpContext.Current.Request.UserHostAddress;
            MvcApplication.logger.Error($"No passcode: [IP] {ip}, [sender] {sender}, [receiver] {receiver}, [subject] {subject}, [appId] {appId}");
            AmazonMail.Modules.SendMail.Send(sender, receiver, subject, content, appId);
            return Error.GetStringError(Constants.ERR_10004_CODE, Constants.ERR_10004_MESSAGE, "Compruebe el código de seguridad");

        }

        [WebMethod]
        public string SendRaw(string strSecureCode, string sender, string receiver, string subject, string content, byte[] message, string appId)
        {
            string ip = HttpContext.Current.Request.UserHostAddress;
            if (strSecureCode != "sp082b") //comprobar el código de seguridad
            {
                MvcApplication.logger.Error($"Invalid passcode: [IP] {ip}, [sender] {sender}, [receiver] {receiver}, [subject] {subject}, [passcode] {strSecureCode}, [appId] {appId}");
                return Error.GetStringError(Constants.ERR_10004_CODE, Constants.ERR_10004_MESSAGE, "Compruebe el código de seguridad");
            }
            if (!AmazonMail.Modules.SendMail.ValidateIP(ip))
            {
                MvcApplication.logger.Error($"IP not allowed: [IP] {ip}, [sender] {sender}, [receiver] {receiver}, [subject] {subject}, [passcode] {strSecureCode}, [appId] {appId}");
                return Error.GetStringError(Constants.ERR_10004_CODE, Constants.ERR_10004_MESSAGE, "Acceso desde una IP no permitida");

            }
            if (!AmazonMail.Modules.SendMail.ValidateApp(appId))
            {
                MvcApplication.logger.Error($"Application not allowed: [IP] {ip}, [sender] {sender}, [receiver] {receiver}, [subject] {subject}, [passcode] {strSecureCode}, [appId] {appId}");
                return Error.GetStringError(Constants.ERR_10004_CODE, Constants.ERR_10004_MESSAGE, "Acceso desde una aplicación no permitida");

            }
            return AmazonMail.Modules.SendMail.SendRaw(sender, receiver, subject, content, message, appId);
        }
    }
}
