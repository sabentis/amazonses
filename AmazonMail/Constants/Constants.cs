﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazonMail
{
    public class Constants
    {
        public const string ERR_10001_CODE = "10001";
        public const string ERR_10001_MESSAGE = "Error no controlado";

        public const string ERR_10002_CODE = "10002";
        public const string ERR_10002_MESSAGE = "Mensaje rechazado";

        public const string ERR_10003_CODE = "10003";
        public const string ERR_10003_MESSAGE = "Destinatario en lista negra";

        public const string ERR_10004_CODE = "10004";
        public const string ERR_10004_MESSAGE = "Código de seguridad no válido";

        public const string ERR_10005_CODE = "10005";
        public const string ERR_10005_MESSAGE = "Dirección IP no permitida";

        public const string ERR_10006_CODE = "10006";
        public const string ERR_10006_MESSAGE = "Aplicación no permitida";



    }
}