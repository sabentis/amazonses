﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AmazonMail.Startup))]
namespace AmazonMail
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
