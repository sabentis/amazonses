﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using AmazonMail.Models;
using NewRelic.Api.Agent;

namespace AmazonMail.Modules
{
    public static class SendMail
    {       
        public static string Send(string sender,
            string receiver,
            string subject,
            string content,
            //List<string> attachments,
            string appId)
        {
            try
            {
                //sender = "mgrigorieva@sabentis.com";

                  // throw new WebServiceBusinessRuleCheckException(UPC.SF.Utilities.Language.TranslateText("BUZONES_MENSAJE_CORREONOHABILITADO"));              

                // Dirección del remite.
                Destination destination = new Destination();
                destination = new Destination { ToAddresses = new List<string> { receiver } };
                               
                // Create the subject and body of the message.
                Content subjectContent  = new Content(subject);
                //revisar la codificación UTF8
                Content textBody = new Content(content);
                Body bodyContent = new Body(textBody);

                // Create a message with the specified subject and body.
                Message message = new Message(subjectContent, bodyContent);

                

                // Assemble the email.
                SendEmailRequest request = new SendEmailRequest(sender, destination, message);

                // Choose the AWS region of the Amazon SES endpoint you want to connect to. Note that your sandbox 
                // status, sending limits, and Amazon SES identity-related settings are specific to a given 
                // AWS region, so be sure to select an AWS region in which you set up Amazon SES. Here, we are using 
                // the US West (Oregon) region. Examples of other regions that Amazon SES supports are USEast1 
                // and EUWest1. For a complete list, see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html 
                Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USEast1;

                // Instantiate an Amazon SES client, which will make the service call.

                AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(REGION);
                // Send the email.              
                SendEmailResponse result = client.SendEmail(request);
                //guardar el id de mensaje
                RegisterMessage(result.MessageId,sender,receiver,subject, content,DateTime.Now, appId);
                return ("0");
                
            }
            catch (Exception ex)
            {
                NewRelic.Api.Agent.NewRelic.NoticeError(ex);
                if (ex is Amazon.SimpleEmail.Model.MessageRejectedException)
                {                    
                    MessageRejectedException mex = (MessageRejectedException)ex;
                    return Error.GetStringError(Constants.ERR_10002_CODE, Constants.ERR_10002_MESSAGE, mex.Message);
                }
                else
                    return Error.GetStringError(ex);
            }    
        }
             
        

        public static string SendRaw(string sender,
            string receiver,
            string subject, string content, byte[]message, string appId)
        {
            try
            {
               

                // Dirección del remite.
                Destination destination = new Destination();
                destination = new Destination { ToAddresses = new List<string> { receiver } };

                //comprobar que el destinatario no esté en la lista negra por hard bounce o spam complaint

                foreach(string r in destination.ToAddresses)
                    if(Queue.AlreadyInBlackList(r))
                        //return Error.GetStringError(Constants.ERR_10003_CODE, Constants.ERR_10003_MESSAGE, r); 
                    {
                        //registramos el mensaje con error
                        RegisterMessage(Guid.NewGuid().ToString(), sender, receiver, subject, content, DateTime.Now, appId, "Black List", "BL");
                        return Error.GetStringError(Constants.ERR_10003_CODE, Constants.ERR_10003_MESSAGE, r); 
                    }

                Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USEast1;

                // Instantiate an Amazon SES client, which will make the service call.

                AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(REGION);

                RawMessage rawMessage= new RawMessage { Data = new MemoryStream(message) };                  
                
                var sendRequest = new SendRawEmailRequest(rawMessage);// { RawMessage = new RawMessage { Data = new MemoryStream(message) } };
                
                var response = client.SendRawEmail(sendRequest);
                RegisterMessage(response.MessageId, sender, receiver, subject, content, DateTime.Now, appId);                           
                //return(response.MessageId);
                return null;
                
            }

            catch (Exception ex)
            {
                NewRelic.Api.Agent.NewRelic.NoticeError(ex);
                if (ex is Amazon.SimpleEmail.Model.MessageRejectedException)
                {
                    MessageRejectedException mex = (MessageRejectedException)ex;
                    return Error.GetStringError(Constants.ERR_10002_CODE, Constants.ERR_10002_MESSAGE, mex.Message);
                }
                else
                    return Error.GetStringError(Constants.ERR_10001_CODE, Constants.ERR_10001_MESSAGE, ex.Message);
            }    
        }

        private static void RegisterMessage(string messageId, string sender, string receiver, string subject, string content, DateTime sendDate, string appId, string strErrorMessage=null, string strErrorType=null)
        {
            try
            {
                using (var db = new AmazonDBModel())
                {
                    MESSAGELOG log = new MESSAGELOG();
                    log.MESSAGEID = messageId;
                    log.SENDER = sender;
                    log.RECEIVER = receiver;
                    log.SUBJECT = subject;
                    log.SENDDATE = sendDate;
                    log.APPID = appId;
                    log.ERRORMESSAGE = strErrorMessage;
                    log.ERRORTYPE = strErrorType;
                    log.MESSAGE = content;
                    db.MESSAGELOG.Add(log);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static bool ValidateIP(string ip)
        {
            string whitelist = ConfigurationManager.AppSettings["IP_WHITELIST"];
            if (string.IsNullOrEmpty(whitelist))
            {
                return true;
            }
            Match m = Regex.Match(ip, whitelist, RegexOptions.IgnoreCase);
            if (m.Success) return true;
            return false;
        }

        public static bool ValidateApp(string appId)
        {
            string whitelist = ConfigurationManager.AppSettings["APP_WHITELIST"];
            if (string.IsNullOrEmpty(whitelist))
            {
                return true;
            }
            Match m = Regex.Match(appId, whitelist, RegexOptions.IgnoreCase);
            if (m.Success) return true;
            return false;
        }
    }
}