﻿using NewRelic.Api.Agent;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Configuration;
using System.IO;

namespace AmazonMail.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public class Log
    {
        /// </summary>
        protected ILogger Logger;


        public Logger GetLogger()
        {
            return SetupLogFactory().GetLogger("app");
        }
        protected LogFactory SetupLogFactory()
        {
            var logFactory = new LogFactory();

            var config = new LoggingConfiguration();

            var fileTarget = new FileTarget("File");
            fileTarget.FileName = GetLogLocation("AppLog", "applog.log");

            fileTarget.Layout = @">>>>>>>>>>>>>> LEVEL: ${level:uppercase=true} <<<<<<<<<<<<<<${newline}
DATE: ${longdate}${newline}
LOGGER: ${logger}${newline}
MESSAGE: ${message}${newline}
PROPERTIES: ${all-mdlc-properties}${newline}
EVENT PROPERTIES: ${all-event-properties}${newline}
PRIME REQUEST: ${prime-request}${newline}";

            fileTarget.ArchiveAboveSize = 1024 * 1024 * 10;
            fileTarget.EnableArchiveFileCompression = true;

            config.AddTarget("file", fileTarget);

            var rule1 = new LoggingRule("*", LogLevel.Trace, fileTarget);
            config.LoggingRules.Add(rule1);
            logFactory.Configuration = config;

            return logFactory;
        }

        private string GetLogLocation(string subFolder, string logName)
        {
            string configuredLogFilePath = ConfigurationManager.AppSettings["LogFolder"];

            string dir = Path.Combine(configuredLogFilePath, subFolder);
            string strFileName = logName + "_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return Path.Combine(dir, strFileName);
        }
    }

}