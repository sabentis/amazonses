﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;


using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon.SQS.Model;

using AmazonMail.Models;
using System.Threading;
using System.Threading.Tasks;
using NewRelic.Api.Agent;
using Z.EntityFramework.Plus;
using NLog;

namespace AmazonMail.Modules
{
    public static class Queue
    {

        /// <summary>Represents the bounce or complaint notification stored in Amazon SQS.</summary>
        /// 
        class AmazonSesMail
        {
            public string MessageId { get; set; }
        }
        class AmazonSqsNotification
        {
            public string Type { get; set; }
            public string Message { get; set; }
        }

        /// <summary>Represents an Amazon SES bounce notification.</summary>
        class AmazonSesBounceNotification
        {
            public string NotificationType { get; set; }
            public AmazonSesBounce Bounce { get; set; }
            public AmazonSesMail Mail { get; set; }
        }
        /// <summary>Represents meta data for the bounce notification from Amazon SES.</summary>
        class AmazonSesBounce
        {

            public string BounceType { get; set; }
            public string BounceSubType { get; set; }
            public DateTime Timestamp { get; set; }
            public List<AmazonSesBouncedRecipient> BouncedRecipients { get; set; }
        }

        /// <summary>Represents the email address of recipients that bounced
        /// when sending from Amazon SES.</summary>
        class AmazonSesBouncedRecipient
        {
            public string EmailAddress { get; set; }
        }
        class AmazonSesComplaintNotification
        {
            public string NotificationType { get; set; }
            public AmazonSesComplaint Complaint { get; set; }
            public AmazonSesMail Mail { get; set; }
        }
        /// <summary>Represents the email address of individual recipients that complained 
        /// to Amazon SES.</summary>
        class AmazonSesComplainedRecipient
        {
            public string EmailAddress { get; set; }
        }
        /// <summary>Represents meta data for the complaint notification from Amazon SES.</summary>
        class AmazonSesComplaint
        {
            public string complaintFeedbackType { get; set; }
            public List<AmazonSesComplainedRecipient> ComplainedRecipients { get; set; }
            public DateTime Timestamp { get; set; }

        }
        class AmazonSesDeliveryNotification
        {
            public string NotificationType { get; set; }
            public AmazonSesDelivery Delivery { get; set; }
            public AmazonSesMail Mail { get; set; }
        }
        /// <summary>Represents the email address of individual recipients that complained 
        /// to Amazon SES.</summary>
        class AmazonSesDeliveryRecipient
        {
            public string EmailAddress { get; set; }
        }
        /// <summary>Represents meta data for the complaint notification from Amazon SES.</summary>
        class AmazonSesDelivery
        {
            public List<AmazonSesDeliveryRecipient> DeliveryRecipients { get; set; }
            public DateTime Timestamp { get; set; }

        }

        /// <summary>Process bounces received from Amazon SES via Amazon SQS.</summary>
        /// <param name="response">The response from the Amazon SQS bounces queue 
        /// to a ReceiveMessage request. This object contains the Amazon SES  
        /// bounce notification.</param> 
        [Transaction]
        private static void ProcessQueuedBounce(ReceiveMessageResponse response)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:ProcessQueuedBounce");

            int messages = response.Messages.Count;

            if (messages > 0)
            {
                foreach (var m in response.Messages)
                {
                    // First, convert the Amazon SNS message into a JSON object.
                    var notification = Newtonsoft.Json.JsonConvert.DeserializeObject<AmazonSqsNotification>(m.Body);

                    // Now access the Amazon SES bounce notification.
                    var bounce = Newtonsoft.Json.JsonConvert.DeserializeObject<AmazonSesBounceNotification>(notification.Message);

                    if (bounce.Bounce == null) continue;
                    switch (bounce.Bounce.BounceType)
                    {
                        //Si es soft bounce ver cuantas veces ha ocurrudo con este usuario, si >3 => blacklist 

                        case "Transient":
                            UpdateMessageLog(bounce.Mail.MessageId, "SB", notification.Message);
                            foreach (var recipient in bounce.Bounce.BouncedRecipients)
                            {
                                if (SoftBounces(recipient.EmailAddress))
                                {
                                    MoveToBlackList(recipient.EmailAddress, bounce.Bounce.Timestamp, String.Format("Bounce ({0}, {1})", bounce.Bounce.BounceType, bounce.Bounce.BounceSubType));
                                }
                            }
                            break;
                        default: //para General y Undetermined Bounce                      
                                 // Remove all recipients that generated a permanent bounce 
                                 // or an unknown bounce. 
                            UpdateMessageLog(bounce.Mail.MessageId, "HB", notification.Message);
                            foreach (var recipient in bounce.Bounce.BouncedRecipients)
                            {                                
                                MoveToBlackList(recipient.EmailAddress, bounce.Bounce.Timestamp, String.Format("Bounce ({0}, {1})", bounce.Bounce.BounceType, bounce.Bounce.BounceSubType));
                            }
                            break;
                    }
                    DeleteNotificationFromQueue(ConfigurationManager.AppSettings["BouncesQueueURL"], m.ReceiptHandle);
                }
            }
        }
        [Transaction]
        public static void ProcessBouncesQueue(string queueURL)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:ProcessBouncesQueue");

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();

            receiveMessageRequest.QueueUrl = queueURL;
            receiveMessageRequest.MaxNumberOfMessages = 10;

            Amazon.SQS.AmazonSQSClient amazonSQSClient = new Amazon.SQS.AmazonSQSClient();

            ReceiveMessageResponse receiveMessageResponse =
            amazonSQSClient.ReceiveMessage(receiveMessageRequest);

            ProcessQueuedBounce(receiveMessageResponse);
        }

        /// <summary>Process complaints received from Amazon SES via Amazon SQS.</summary>
        /// <param name="response">The response from the Amazon SQS complaint queue 
        /// to a ReceiveMessage request. This object contains the Amazon SES 
        /// complaint notification.</param>
        [Transaction]
        private static void ProcessQueuedComplaint(ReceiveMessageResponse response)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:ProcessQueuedComplaint");

            int messages = response.Messages.Count;

            if (messages > 0)
            {
                foreach (var message in response.Messages)
                {
                    var notification = Newtonsoft.Json.JsonConvert.DeserializeObject<AmazonSqsNotification>(message.Body);

                    var complaint = Newtonsoft.Json.JsonConvert.DeserializeObject<AmazonSesComplaintNotification>(notification.Message);
                    if (complaint.Complaint == null) continue;
                    UpdateMessageLog(complaint.Mail.MessageId, "SC", String.Format("Complaint ({0})", complaint.Complaint.complaintFeedbackType));

                    foreach (var recipient in complaint.Complaint.ComplainedRecipients)
                    {
                        MoveToBlackList(recipient.EmailAddress, complaint.Complaint.Timestamp, "spam complaint");
                    }
                    //si todo ha ido bien borramos la notificación de la cola
                    DeleteNotificationFromQueue(ConfigurationManager.AppSettings["ComplaintsQueueURL"], message.ReceiptHandle);
                }
            }
        }
        [Transaction]
        public static void ProcessComplaintsQueue(string queueURL)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:ProcessComplaintsQueue");

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();

            receiveMessageRequest.QueueUrl = queueURL;
            receiveMessageRequest.MaxNumberOfMessages = 10;

            Amazon.SQS.AmazonSQSClient amazonSQSClient = new Amazon.SQS.AmazonSQSClient();


            ReceiveMessageResponse receiveMessageResponse =
            amazonSQSClient.ReceiveMessage(receiveMessageRequest);

            ProcessQueuedComplaint(receiveMessageResponse);
        }

        /// <summary>Process deliveries received from Amazon SES via Amazon SQS.</summary>
        /// <param name="response">The response from the Amazon SQS deliveries queue 
        /// to a ReceiveMessage request. This object contains the Amazon SES 
        /// delivery notification.</param>
        [Transaction]
        private static void ProcessQueuedDelivery(ReceiveMessageResponse response)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:ProcessQueuedDelivery");

            int messages = response.Messages.Count;

            if (messages > 0)
            {
                foreach (var message in response.Messages)
                {
                    var notification = Newtonsoft.Json.JsonConvert.DeserializeObject<AmazonSqsNotification>(message.Body);

                    var delivery = Newtonsoft.Json.JsonConvert.DeserializeObject<AmazonSesDeliveryNotification>(notification.Message);
                    if (delivery.Delivery == null) continue;
                    UpdateMessageLog(delivery.Mail.MessageId, "OK", "");

                    //si todo ha ido bien borramos la notificación de la cola
                    DeleteNotificationFromQueue(ConfigurationManager.AppSettings["DeliveriesQueueURL"], message.ReceiptHandle);
                }
            }
        }
        [Transaction]
        public static void ProcessDeliveriesQueue(string queueURL)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:ProcessDeliveriesQueue");

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();

            receiveMessageRequest.QueueUrl = queueURL;
            receiveMessageRequest.MaxNumberOfMessages = 10;

            Amazon.SQS.AmazonSQSClient amazonSQSClient = new Amazon.SQS.AmazonSQSClient();


            ReceiveMessageResponse receiveMessageResponse =
            amazonSQSClient.ReceiveMessage(receiveMessageRequest);

            ProcessQueuedDelivery(receiveMessageResponse);
        }
        [Transaction]
        public static void RefreshAll()
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:RefreshAll");
           
             //no procesar colas en modo de desarrollo
            if (ConfigurationManager.AppSettings["DebugMode"] == "1")
            {
                MvcApplication.logger.Info("### RefreshAll not available in debugMode ###");
                return;
            }
            /*Parallel.For(0, 10, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
            {
                ProcessDeliveriesQueue(ConfigurationManager.AppSettings["DeliveriesQueueURL"]);               
            }); */
            MvcApplication.logger.Info("### RefreshAll started ###");
            for (int i = 0; i < 10; i++)
            {
                ProcessDeliveriesQueue(ConfigurationManager.AppSettings["DeliveriesQueueURL"]);
            }

            ProcessBouncesQueue(ConfigurationManager.AppSettings["BouncesQueueURL"]);
            ProcessComplaintsQueue(ConfigurationManager.AppSettings["ComplaintsQueueURL"]);
            MvcApplication.logger.Info("### RefreshAll finished ###");
        }

        [Transaction]
        private static void DeleteNotificationFromQueue(string queueURL, string receiptHandle)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:DeleteNotificationFromQueue");

            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest();

            deleteMessageRequest.QueueUrl = queueURL;
            deleteMessageRequest.ReceiptHandle = receiptHandle;

            Amazon.SQS.AmazonSQSClient amazonSQSClient = new Amazon.SQS.AmazonSQSClient();
            amazonSQSClient.DeleteMessage(deleteMessageRequest);
        }
        [Trace]
        public static void UpdateMessageLog(string messageId, string errorType, string errorMessage)
        {
            using (var db = new AmazonDBModel())
            {
                MESSAGELOG message = (from m in db.MESSAGELOG where m.MESSAGEID == messageId select m).FirstOrDefault();
                if (message == null) return;
                message.ERRORMESSAGE = errorMessage;
                message.ERRORTYPE = errorType;
                db.MESSAGELOG.Attach(message);
                db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
        [Trace]
        public static void MoveToBlackList(string email, DateTime timestamp, string reason)
        {
            if (!AlreadyInBlackList(email))
            {
                using (var db = new AmazonDBModel())
                {
                    BLACKLIST entry = new BLACKLIST();
                    entry.EMAIL = email;
                    entry.TIMESTAMP = timestamp;
                    entry.REASON = reason;
                    db.BLACKLIST.Add(entry);
                    db.SaveChanges();
                }
            }
        }

        //si x intentos previos en un perodo de tiempo han resultado en Soft Bounce enviar el destinatario a la lista negra
        [Trace]
        public static bool SoftBounces(string email)
        {
            using (var db = new AmazonDBModel())
            {
                //DateTime limit = System.DateTime.Today.AddDays(-7);
                DateTime limit = System.DateTime.Today.AddDays(-(Convert.ToInt32(ConfigurationManager.AppSettings["ErrorsMaxTime"])));
                int maxErrors = Convert.ToInt32(ConfigurationManager.AppSettings["ErrorsMaxCount"]);
                return ((from b in db.MESSAGELOG where b.RECEIVER == email && b.SENDDATE >= limit && b.ERRORTYPE == "SB" select b).Count() >= maxErrors);
            }
        }

        [Trace]
        public static bool AlreadyInBlackList(string email)
        {
            using (var db = new AmazonDBModel())
            {
                return (from b in db.BLACKLIST where b.EMAIL == email select b).Any();
            }
        }
        [Trace]
        public static void CleanOldMessages()
        {
            using (var db = new AmazonDBModel())
            {
                DateTime limit = DateTime.Now.AddDays(-90);
                var oldmessages = db.MESSAGELOG.Where(m => m.SENDDATE <= limit);
                /*foreach(var m in oldmessages)
                { 
                    MESSAGELOG messagelog = db.MESSAGELOG.Find(m.LOGID);
                    db.MESSAGELOG.Remove(messagelog);
                }
                db.SaveChanges(); */
                string strBatchSize = ConfigurationManager.AppSettings["BatchSize"];
                int intBatchSize;
                bool isInteger = System.Int32.TryParse(strBatchSize, out intBatchSize);
                if (!isInteger) intBatchSize = 100;
                db.BulkDelete(oldmessages, options => options.BatchSize = intBatchSize);
            }
        }

        //eliminar de la lista negra las direcciones que han entrado por SoftBounce pasada una semana
        [Trace]
        public static void CleanBlackList()
        {
            using (var db = new AmazonDBModel())
            {
                DateTime limit = DateTime.Now.AddDays(-(Convert.ToInt32(ConfigurationManager.AppSettings["QuarantineDays"])));
                var emails = db.BLACKLIST.Where(m => m.TIMESTAMP <= limit && m.REASON.Contains("Transient"));
                string strBatchSize = ConfigurationManager.AppSettings["BatchSize"];
                int intBatchSize;
                bool isInteger = System.Int32.TryParse(strBatchSize, out intBatchSize);
                if (!isInteger) intBatchSize = 100;
                db.BulkDelete(emails, options => options.BatchSize = intBatchSize);

            }
        }
    }
}
