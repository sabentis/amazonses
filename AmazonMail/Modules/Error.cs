﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using AmazonMail;

namespace AmazonMail.Modules
{
    public static class Error
    {          
       
        public static string GetStringError(Exception e)
        {
            return GetStringError(Constants.ERR_10001_CODE, Constants.ERR_10001_MESSAGE, e.Message);             
        }       
       
        public static string GetStringError(string errorcode, string errortype, string message)
        {
            return (String.Format("ERR{0} - {1}: {2}", errorcode, errortype, message));
        }  
    }  
}