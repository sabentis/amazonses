//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AmazonMail.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MESSAGELOG
    {
        public int LOGID { get; set; }
        public string MESSAGEID { get; set; }
        public string SENDER { get; set; }
        public string RECEIVER { get; set; }
        public System.DateTime SENDDATE { get; set; }
        public string APPID { get; set; }
        public string ERRORMESSAGE { get; set; }
        public string SUBJECT { get; set; }
        public string ERRORTYPE { get; set; }
        public string MESSAGE { get; set; }
    }
}
