﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AmazonMail.Models;
using AmazonMail.Modules;
using PagedList;

using Microsoft.AspNet.Identity;

namespace AmazonMail.Controllers
{
    public class MessageLogController : Controller
    {
        private AmazonDBModel db = new AmazonDBModel();

     
        
        // GET: /MessageLog/
        public ActionResult Index(string hidUpdate, string sortOrder, string currentFilter, string searchString, int? page)
        {
            if(!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            //string strCurrentUserId=System.Web.HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId();
            string strCurrentUserId = User.Identity.GetUserId();        
            IdentityManager idManager = new IdentityManager();
            string strAppId=idManager.GetUserAppId(strCurrentUserId);
            
            
            if (!String.IsNullOrEmpty(hidUpdate))
            {
               //refresh with info from queue
                Queue.RefreshAll();
                //return View(db.MESSAGELOG.OrderByDescending(m => m.SENDDATE).ToList());
            }
            //return View(db.MESSAGELOG.OrderByDescending(m => m.SENDDATE).ToList());
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date_asc" : "";
            ViewBag.ReceiverSortParm = sortOrder == "receiver" ? "receiver_desc" : "receiver";
            ViewBag.AppidSortParm = sortOrder == "appid" ? "appid_desc" : "appid";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            
            var messages = from m in db.MESSAGELOG select m;
            if (strAppId != "SES")
                messages = messages.Where(s => s.APPID == strAppId);

            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.RECEIVER.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "receiver_desc":
                    messages = messages.OrderByDescending(s => s.RECEIVER);
                    break;
                case "receiver":
                    messages = messages.OrderBy(s => s.RECEIVER);
                    break;
                case "appid_desc":
                    messages = messages.OrderByDescending(s => s.APPID);
                    break;
                case "appid":
                    messages = messages.OrderBy(s => s.APPID);
                    break;
                case "date_asc":
                    messages = messages.OrderBy(s => s.SENDDATE);
                    break;
                default:
                    messages = messages.OrderByDescending(s => s.SENDDATE);
                    break;  
            }

            int pageSize = 50;
            int pageNumber = (page ?? 1);
            var mes=messages.ToPagedList(pageNumber, pageSize);

            //calculo las estadisticas
            ViewBag.numSoftBounces = messages.Where(m => m.ERRORTYPE == "SB").Count();
            ViewBag.numHardBounces = messages.Where(m => m.ERRORTYPE == "HB").Count();
            ViewBag.numSpamComplaints = messages.Where(m => m.ERRORTYPE == "SC").Count();
            ViewBag.numOK = messages.Where(m => m.ERRORTYPE == "OK").Count();
            ViewBag.numTotal = messages.Count();
            ViewBag.numBlackListed = messages.Where(m => m.ERRORTYPE == "BL").Count(); 

            return View(mes);
            //return View(messages.ToList());
        }

        // GET: /MessageLog/Details/5
        public ActionResult Details(int? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MESSAGELOG messagelog = db.MESSAGELOG.Find(id);
            if (messagelog == null)
            {
                return HttpNotFound();
            }
            return View(messagelog);
        }

        // GET: /MessageLog/Create
        public ActionResult Create()
        {
            return View();
           
        }

        // POST: /MessageLog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="LOGID,MESSAGEID,SENDER,RECEIVER,SENDDATE,APPID,ERRORMESSAGE,SUBJECT")] MESSAGELOG messagelog)
        {
            if (ModelState.IsValid)
            {
                db.MESSAGELOG.Add(messagelog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messagelog);
        }

        // GET: /MessageLog/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MESSAGELOG messagelog = db.MESSAGELOG.Find(id);
            if (messagelog == null)
            {
                return HttpNotFound();
            }
            return View(messagelog);
        }

        // POST: /MessageLog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="LOGID,MESSAGEID,SENDER,RECEIVER,SENDDATE,APPID,ERRORMESSAGE,SUBJECT")] MESSAGELOG messagelog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messagelog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messagelog);
        }

        // GET: /MessageLog/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MESSAGELOG messagelog = db.MESSAGELOG.Find(id);
            if (messagelog == null)
            {
                return HttpNotFound();
            }
            return View(messagelog);
        }

        // POST: /MessageLog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MESSAGELOG messagelog = db.MESSAGELOG.Find(id);
            db.MESSAGELOG.Remove(messagelog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        } 

    }   
}


