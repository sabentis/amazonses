﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AmazonMail.Models;
using PagedList;



namespace AmazonMail.Controllers
{
    public class BlackListController : Controller
    {
        private AmazonDBModel db = new AmazonDBModel();

        // GET: /BlackList/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date_asc" : "";
            ViewBag.ReceiverSortParm = sortOrder == "receiver" ? "receiver_desc" : "receiver";         
        
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var blacklist = from b in db.BLACKLIST select b;
         
            if (!String.IsNullOrEmpty(searchString))
            {
                blacklist = blacklist.Where(s => s.EMAIL.Contains(searchString)
                                       || s.EMAIL.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "receiver_desc":
                    blacklist = blacklist.OrderByDescending(s => s.EMAIL);
                    break;
                case "receiver":
                    blacklist = blacklist.OrderBy(s => s.EMAIL);
                    break;              
                case "date_asc":
                    blacklist = blacklist.OrderBy(s => s.EMAIL);
                    break;
                default:
                    blacklist = blacklist.OrderByDescending(s => s.EMAIL);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);            
            var mes = blacklist.ToPagedList(pageNumber, pageSize);
            return View(mes);            
        }

        
        public ActionResult Search(string currentFilter, string searchString, int? page)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        public ActionResult Find(string currentFilter, string searchString, int? page)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            searchString = searchString.Trim().ToLower().Replace(Environment.NewLine, "").Replace(" ", "");
            ViewBag.CurrentFilter = searchString;
            var emails = searchString.Split(',');
            var blacklist = db.BLACKLIST.Where(b => emails.Contains(b.EMAIL.Trim().ToLower()));
            blacklist = blacklist.OrderByDescending(s => s.EMAIL);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var mes = blacklist.ToPagedList(pageNumber, pageSize);
            return View(mes);         
            
        }

        // GET: /BlackList/Details/5
        public ActionResult Details(int? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BLACKLIST blacklist = db.BLACKLIST.Find(id);
            if (blacklist == null)
            {
                return HttpNotFound();
            }
            return View(blacklist);
        }

        // GET: /BlackList/Create
        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        // POST: /BlackList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,EMAIL,TIMESTAMP,REASON")] BLACKLIST blacklist)
        {
            if (ModelState.IsValid)
            {
                db.BLACKLIST.Add(blacklist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(blacklist);
        }



       

    // POST: /BlackList/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,EMAIL,TIMESTAMP,REASON")] BLACKLIST blacklist)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            if (ModelState.IsValid)
            {
                db.Entry(blacklist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blacklist);
        }

        // GET: /BlackList/Delete/5
        public ActionResult DeleteAll(string mailIdentifiers)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            if (mailIdentifiers == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var emails = mailIdentifiers.Split(',');
            var blacklist = db.BLACKLIST.Where(b => emails.Contains((b.ID.ToString())));
            db.BulkDelete(blacklist, options => options.BatchSize = 100);
            db.SaveChanges();  
            return RedirectToAction("Search");
        }

        public ActionResult Delete(int? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BLACKLIST blacklist = db.BLACKLIST.Find(id);
            if (blacklist == null)
            {
                return HttpNotFound();
            }
            return View(blacklist);
        }

        // POST: /BlackList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            BLACKLIST blacklist = db.BLACKLIST.Find(id);
            db.BLACKLIST.Remove(blacklist);
            db.SaveChanges();
            if (User.IsInRole("Admin"))
                return RedirectToAction("Index");
            else
                return RedirectToAction("Search");
        }            

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
