﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AmazonMail.Modules;
using System.Timers;
using System.Configuration;
using NewRelic.Api.Agent;
using NLog;
using System.Net;

namespace AmazonMail
{
    public class MvcApplication : System.Web.HttpApplication
    {

        public static System.Timers.Timer refreshtimer = new System.Timers.Timer();
        public static System.Timers.Timer cleanuptimer = new System.Timers.Timer();

        public static Log log = new Log();
        public static ILogger logger = log.GetLogger();

        [Transaction]
        protected void Application_Start()
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:Application_Start");

            ServicePointManager.Expect100Continue = true;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12
                                                   | SecurityProtocolType.Ssl3;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            deployEntityFrameworkExtensionsLicense();

           //Actualizar colas de bounces y complaints
            //Queue.RefreshAll();
            //hacer limpieza de mensajes antiguos
           
            refreshtimer.Elapsed += new ElapsedEventHandler(RefreshQueue);
            string strTimeout = ConfigurationManager.AppSettings["UpdateTimeout"];
            int timeout;
            bool isInteger = System.Int32.TryParse(strTimeout, out timeout);
            if (!isInteger) timeout = 5;
            refreshtimer.Interval = timeout * 60000; //actualización automática: timeout * 1 minuto (60 segundos * 1000 milisegundos) 
            refreshtimer.AutoReset = true;
            refreshtimer.Enabled = true;
            refreshtimer.Start();

            schedule_CleanupTimer();
        }
        [Transaction]
        protected void RefreshQueue(Object sender, ElapsedEventArgs e)
        {
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:RefreshQueue");

            refreshtimer.Stop();
            logger.Info("### Refresh Timer stopped ###");
            //refreshtimer.Interval = ((1000 * 60) * FrecuenciaAlertasEnMinutos) * 0.90;
            Queue.RefreshAll();
            logger.Info("### Refresh Timer starting ###");
            refreshtimer.Start();
            return;
        }

        static void schedule_CleanupTimer()
        {
           /*log = new Log();
           logger=log.GetLogger();*/            

            logger.Info("### Cleanup Timer Started ###");
            
         
            DateTime nowTime = DateTime.Now;
            DateTime scheduledTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, 6, 0, 0, 0); //Specify your scheduled time HH,MM,SS [8am and 42 minutes]
            if (nowTime > scheduledTime)
            {
                scheduledTime = scheduledTime.AddDays(1);
            }
            double tickTime = (double)(scheduledTime - DateTime.Now).TotalMilliseconds;
            cleanuptimer = new Timer(tickTime);
            cleanuptimer.Elapsed += new ElapsedEventHandler(Cleanup);
            cleanuptimer.Start();

            
        }

        [Transaction]
        static void Cleanup(object sender, ElapsedEventArgs e)
        {
            /*Log log = new Log();
            ILogger logger = log.GetLogger();*/

            logger.Info("### Cleanup Timer Started ###");
            NewRelic.Api.Agent.NewRelic.SetTransactionName("custom", $"async:Cleanup");
            logger.Info("### Cleanup Timer Stopped ###");
            cleanuptimer.Stop();
            logger.Info("### Cleanup Task Started ###");
            logger.Info("Performing scheduled cleanup task");
            Queue.CleanOldMessages();
            Queue.CleanBlackList();
            logger.Info("### Cleanup Task Finished ###");
            schedule_CleanupTimer();
        }
    

    protected void deployEntityFrameworkExtensionsLicense()
        {
            var licenseName = "MTExMzsxMDEtQ0VScElF";
            var licenseKey = "MTYwNDI4NzktMDZlNi0zNDE5LTY5NWEtMzMwNTczODY0ZjZj";

            licenseName = Base64Decode(licenseName);
            licenseKey = Base64Decode(licenseKey);

            // Set the EntityFrameworkExtensions plugins
            Z.EntityFramework.Extensions.LicenseManager.AddLicense(licenseName, licenseKey);

            string error;
            Z.EntityFramework.Extensions.LicenseManager.ValidateLicense(out error, Z.BulkOperations.ProviderType.SqlServer);
            if (!String.IsNullOrWhiteSpace(error))
            {
                throw new Exception("Unable to deploy Entity Framework Extensions license.");
            }
        }
        
        private System.Text.Encoding Base64StringEncodeDecodeDefaultEncoding
        {
            get
            {
                return System.Text.Encoding.UTF8;
            }
        }
        [Trace]
        private string Base64Encode(string data, System.Text.Encoding enc = null)
        {
            if (enc == null)
                enc = Base64StringEncodeDecodeDefaultEncoding;

            byte[] toEncodeAsBytes = enc.GetBytes(data);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }
        [Trace]
        private string Base64Decode(string encodedData, System.Text.Encoding enc = null)
        {
            if (enc == null)
                enc = Base64StringEncodeDecodeDefaultEncoding;

            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = enc.GetString(encodedDataAsBytes);
            return returnValue;
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();            
            NewRelic.Api.Agent.NewRelic.NoticeError(ex);            
        }
    }

}
