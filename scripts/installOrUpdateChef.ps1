$WorkingDir = Convert-Path .
$ZipPath = Join-Path $WorkingDir '\chef_cmdlet.zip'
(New-Object Net.WebClient).DownloadFile('https://ci.appveyor.com/api/projects/David19767/iischef/artifacts/iischef.cmdlet.zip?branch=1.x', $ZipPath)
$DESTINATION= Join-Path $env:ProgramFiles "\WindowsPowerShell\Modules\Chef"
New-Item -ItemType directory -Force -Path $DESTINATION
(new-object -com shell.application).namespace($DESTINATION).CopyHere((new-object -com shell.application).namespace($ZipPath).Items(),16)
Remove-Item $ZipPath