echo '--- # Artifact Settings'
echo "appveyor-project-id: $($args[0])"
echo "appveyor-project-name: $($args[1])"
echo "build-folder: $($args[2])"
echo "build-id: $($args[3])"
echo "build-date: $((Get-Date).ToString('yyyy-MM-ddTH:mm:ss'))"
echo "repo-branch: $($args[4])"
echo "repo-name: $($args[5])"
echo "repo-is-tag: $($args[6])"
echo "repo-commit: $($args[7])"
echo "repo-commit-author: $($args[8])"
echo "repo-commit-author-email: $($args[9])"
echo "repo-commit-timestamp: $($args[10])"

$root= git branch -r --points-at refs/remotes/origin/HEAD | Select-String '\->' 
#Equivalen a los cut
$rootArr= $root -split ' '
$root = $rootArr[4]
$rootArr= $root -split '/'
$root= $rootArr[1]

echo "repo-branch-default: $($root)"

$current_branch= git rev-parse --abbrev-ref HEAD

$firsCommit= git rev-list --max-parents=0 HEAD
$resout=$current_branch
$acumulacion= $current_branch
$original= $current_branch

##############
#while($original -eq $current_branch)
#{
	[string]$parent_commit_number= git log $parent_commit_number --first-parent --pretty=format:"%P" -1
	$temp= $parent_commit_number -split " "
	$parent_commit_number= $temp[0]
#	echo "parent_commit_number $($parent_commit_number)"
	$parent_commit_branch_list= git branch --contains $parent_commit_number	
#	echo "parent_commit_branch_list $($parent_commit_branch_list)"
	
	$parent_commit_branch_list= git branch --contains $parent_commit_number | Select-String -pattern $acumulacion -notMatch 
#	echo "parent_commit_branch_list_filtrado $($parent_commit_branch_list)"
	foreach($b in $parent_commit_branch_list)
	{	
		[string]$branch = $b
		$branch = $branch.replace(" ","")
#		echo "Is $($branch)  ancestro de $($current_branch) ??"
		git merge-base --is-ancestor $branch $current_branch
		if($LASTEXITCODE -eq 0){
#			echo "$($current_branch) ha enonctrado a su padre: $($branch)"
			$current_branch = $branch
			$acumulacion+="|"+$current_branch
			$resout+=", "+$current_branch
		}	
	}	
#}
###############

echo "#repo-branch-parent: $($current_branch)"	
echo "#repo-branch-ancestry: $($resout)"















